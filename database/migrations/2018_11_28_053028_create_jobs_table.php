<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('category');
            $table->string('salary');
            $table->string('location');
            $table->string('position_type');
            $table->string('gender');
            $table->text('description');
            $table->string('company_name')->nullable();
            $table->string('company_website')->nullable();
            $table->unsignedInteger('user_id');
            $table->timestamps();
        });

        // Pivot table
        Schema::create('job_user', function (Blueprint $table) {
            $table->unsignedInteger('job_id');
            $table->unsignedInteger('user_id');
            $table->string('apply_status');

            $table->foreign('job_id')
            ->references('id')->on('jobs')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
