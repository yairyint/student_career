<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'user_type' => 'admin',
        ]);
        DB::table('users')->insert([
            'username' => 'student',
            'email' => 'student@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('students'),
            'user_type' => 'student',
        ]);
        DB::table('users')->insert([
            'username' => 'student1',
            'email' => 'student1@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('students'),
            'user_type' => 'student',
        ]);
        DB::table('users')->insert([
            'username' => 'employer',
            'email' => 'employer@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('employer'),
            'user_type' => 'employer',
        ]);
        DB::table('users')->insert([
            'username' => 'employer1',
            'email' => 'employer1@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('employer'),
            'user_type' => 'employer',
        ]);
    }
}
