<?php

use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'title' => 'Resume Reality',
            'content' => 'A well-written resume WON’T get you the job.   It can happen, but is rare.  The purpose of a résumé is to market you and your skills, to communicate your value and get the attention of the reader (i.e., the hiring manager or recruiter) who then CALLS YOU FOR AN INTERVIEW.',
            'image' => 'image1.jpeg',
            'category_id' => 1,
            'user_id' => 1,
        ]);
        DB::table('posts')->insert([
            'title' => 'The Brand Called ‘Me’',
            'content' => 'What does job-hunting have to do with branding?  Why is creating a ‘Personal Brand’ viewed as such a crucial step in the job search and in career planning overall?',
            'image' => 'image2.jpg',
            'category_id' => 2,
            'user_id' => 1,
        ]);
        DB::table('posts')->insert([
            'title' => 'Bright Outlook for Jobs in Public Relations',
            'content' => 'If you like a fast-paced, exciting environment where every day will be a challenge take a CLOSER LOOK at a career in PR.  Public Relations is an exciting industry because it is important to every business and organization. ',
            'image' => 'job-bg.jpeg',
            'category_id' => 3,
            'user_id' => 1,
        ]);
    }
}
