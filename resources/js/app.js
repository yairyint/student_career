
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// Vue.use(ElementUI);
// elementLocale.use(elementLangEn);

import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
// import { routerHistory, writeHistory } from 'vue-router-back-button'
import ElementUI from 'element-ui'
import { Notification } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import elementLangEn from 'element-ui/lib/locale/lang/en'
import elementLocale from 'element-ui/lib/locale'
import * as TastyBurgerButton from 'vue-tasty-burgers';
import 'vue-tasty-burgers/dist/vue-tasty-burgers.css';
import { VueEditor } from 'vue2-editor'
import routes from './route.js'


elementLocale.use(elementLangEn);
Vue.use(ElementUI)

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('pagination', require('laravel-vue-pagination'));

const router = new VueRouter({
	// mode: 'history',
	history : true,
	routes
});

Vue.use(TastyBurgerButton)
Vue.use(VueRouter)
Vue.router=router
Vue.use(ElementUI)
Vue.axios=axios

axios.defaults.baseURL = 'http://localhost:8000/api';

Vue.use(require('@websanova/vue-auth'), {
    auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

var app = new Vue({
    el: '#app',
    props: ['id'],
    render: h => h(App),
    router,
});