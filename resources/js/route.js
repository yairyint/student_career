import Navigation from './components/layouts/navi-index.vue'
import Navi from './components/layouts/navi.vue'
import AuthNavi from './components/layouts/auth-navi.vue'
import ErrorPage from './components/errorPage.vue'
import HowItWork from './components/HowItWork.vue'
import Dashboard from './components/dashboard.vue'
import UserJoblist from './components/jobs/manageJob.vue'
import JobPost from './components/jobs/JobPost.vue'
import EditJob from './components/jobs/edit.vue'
import JobPage from './components/jobs/JobPage.vue'
import applierList from './components/jobs/applierList.vue'
import searchByTitle from './components/jobs/search.vue'
import Cvform from './components/student_cvforms/create.vue'
import editCvform from './components/student_cvforms/edit.vue'
import loginForm from './components/authentication/login.vue'
import registerForm from './components/authentication/register.vue'
import PostDashboard from './components/posts/index.vue'
import blogPost from './components/posts/blog.vue'
import Category from './components/categories/index.vue'


const router = [
	{
		path : '/',
		component : Navigation,
		redirect : '/',
		children : [
			{
	  			path: '/',
	  			name: 'HowItWork',
	  			component: HowItWork,
				props: true,	
	  		},
	  		{
	  			path: '/404',
	  			name: 'HowItWork',
	  			component: HowItWork,
				props: true,	
	  		},
	  		
		]
	},
	{
		path: '/job',
		component: Navi,
		redirect: '/create',
		children : [
			{
	  			path: '/job/create',
	  			name: 'JobPost',
	  			component: JobPost,
				props: true,
				meta: {
					auth: true
				}
	  		},
	  		{
	  			path: '/job/:id/edit',
	  			name: 'EditJob',
	  			component: EditJob,
	  			props: true,
	  			meta: {
	  				auth: true
	  			}
	  		},
	  		{
	  			path: '/:user_id/jobLists',
	  			name: 'UserJoblist',
	  			component: UserJoblist,
				props: true,
				meta: {
					auth: true
				}
	  		},
	  		{
	  			path: '/joblists',
	  			name: 'JobPage',
	  			component: JobPage,
				props: true,
				meta: {
					auth: true
				}
	  		},
	  		{
	  			path: '/job/:id/show',
	  			name: 'applierList',
	  			component: applierList,
				props: true,
				meta: {
					auth: true
				}
	  		},
	  		{
	  			path: '/joblists/:search',
	  			name: 'searchByTitle',
	  			component: searchByTitle,
				props: true,
				meta: {
					auth: true
				}
	  		},
	  		{
	  			path: '/cvform',
	  			name: 'Cvform',
	  			component: Cvform,
				props: true,
				meta: {
					auth: true
				}
	  		},
	  		{
				path: '/cvform/:id/edit',
				name: 'editCvform',
				component: editCvform,
				props: true,
				meta: {
					auth: true
				}
			},
			{
				path: '/posts',
				name: 'blogPost',
				component: blogPost,
				props: true,
				meta: {
					auth: true
				}
			},
		]
	},
	{
		path : '/auth',
		component : AuthNavi,
		redirect : '/login',
		children : [
			{
				path: '/dashboard',
	  			name: 'Dashboard',
	  			component: Dashboard,
	  			redirect: '/admin/post',
				props: true,
				meta: {
					auth: true
				},
				children: [
					{
						path: '/admin/post',
						name: 'PostDashboard',
			  			component: PostDashboard,
						props: true,
					},
					{
						path: '/admin/catgory',
						name: 'Category',
			  			component: Category,
						props: true,
					},
				]
			},
	  		{
	  			path: '/how_it_work',
	  			name: 'how_it_work',
	  			component: HowItWork,
				props: true,
				
	  		},
	  		{
				path: '/login',
				name: 'login',
				component: loginForm,
				meta: {
					auth: false
				}
			},
			{
				path: '/register',
				name: 'register',
				component: registerForm,
				meta: {
					auth: false
				}
			}
		]
	},
	{ path: '*', component: ErrorPage },
];

export default router