<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('job/{job}', 'API\JobController@show');

Route::group(['middleware' => 'api'], function () {
    Route::post('auth/register', 'AuthController@register');
    Route::post('auth/login', 'AuthController@login');
    Route::post('auth/logout', 'AuthController@logout');
    Route::get('auth/user', 'AuthController@user');

    // Post
    Route::resource('posts', 'API\PostController');

    // Category
    Route::get('categories/allCategories', 'API\CategoryController@allCategories');
    Route::resource('categories', 'API\CategoryController');

    // Job
    Route::get('job/searchByTitle/{search}', 'API\JobController@searchByTitle');
    Route::get('job/searchByLocation/{search}', 'API\JobController@searchByLocation');
    Route::get('job', 'API\JobController@index');
    Route::post('job', 'API\JobController@store');
    Route::get('job/create', 'API\JobController@create');
    Route::get('job/{job}/show', 'API\JobController@show');
    Route::get('job/{job}/show/applierlists', 'API\JobController@applierLists');
    Route::get('job/{job}/edit', 'API\JobController@show');
    Route::patch('job/{job}', 'API\JobController@update');
    Route::delete('job/{job}', 'API\JobController@destroy');
    Route::get('{user_id}/jobLists', 'API\JobController@userJob');

    // CV form
    Route::post('cvform', 'API\CvformController@store');
    Route::get('cvform/{applier_id}/show', 'API\CvformController@show');
    Route::get('cvform/{id}/edit', 'API\CvformController@edit');
    Route::patch('cvform/{id}', 'API\CvformController@update');

    Route::post('applier', 'API\JobController@storeApplierAndJob');
});

Route::group(['middleware' => 'jwt.refresh'], function () {
    Route::get('auth/refresh', 'AuthController@refresh');
});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
