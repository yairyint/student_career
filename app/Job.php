<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use JWTAuth;

class Job extends Model
{

    protected $table = "jobs";
    protected $fillable = ['title', 'category', 'salary', 'location', 'position_type', 'gender', 'description', 'user_id', 'company_name', 'company_website'];

    protected $appends = ['is_applied'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function appliers()
    {
        return $this->belongsToMany('App\User', 'job_user', 'job_id', 'user_id')->withPivot('apply_status');
    }

    public function checkJobAppliedStatus()
    {
        $result = $this->appliers()->where('user_id', Auth::user()->id)->first();

        if ($result == null) {
            $this->attributes['is_applied'] = 'not applied';
        } elseif ($result->pivot->apply_status == 'applied') {
            $this->attributes['is_applied'] = 'Applied';
        } else {
            $this->attributeerrors['is_applied'] = 'error';
        }
    }

    public function getIsAppliedAttribute()
    {
        $result = $this->appliers()->where('user_id', Auth::user()->id)->first();

        if ($result == null) {
            return "Apply Now";
        } elseif ($result->pivot->apply_status == 'applied') {
            return "Applied";
        } else {
            return "error";
        }
    }
}
